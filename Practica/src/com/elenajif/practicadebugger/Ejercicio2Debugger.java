package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector; // Scanner in = new Scanner (System.in);
		int numeroLeido;// faltaria iniciar la variable--> int numeroLeido=0;
		int resultadoDivision; // faltaria iniciar la variable --> int resultadoDivision=0;
		
		lector = new Scanner(System.in);// Como ya tenemos el Scanner declarado al principio del programa no
		//seria necesario volver a iniciarlo
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
				
		lector.close();
	}

}
